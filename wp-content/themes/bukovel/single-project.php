<?php setcookie('viewedProd[' . get_the_ID() . ']', get_the_ID(), time() + 3600, COOKIEPATH, COOKIE_DOMAIN);
get_header(); ?>
    <!-- Content -->

    <div class="content">

        <?
        //получим все нужные данные здесь
        $fields = get_fields();
        // запишем в куку просмотренный проект
        ?>

        <!--  Popus  -->

        <div class="popup-calback">

            <div class="popup-bg"></div>

            <div class="callback-box">

                <div class="close-btn-box">

                    <button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
                    </button>

                </div>

                <div class="callback-content">

                    <div class="sect-h">

                        <h3 class="center-txt">заказ</h3>

                        <h2 class="center-txt green-txt">звонка</h2>

                    </div>

                    <p>Оставьте ваши контактные данные, и наши менеджеры свяжутся с вами в ближайшее время, чтобы
                        обсудить все детали.</p>

                    <div class="callback-form">

                        <form>

                            <div class="input-box">

                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                <input type="text" placeholder="Ваше имя">

                            </div>

                            <div class="input-box">

                                <i class="fa fa-mobile" aria-hidden="true"></i>
                                <input type="tel" placeholder="Ваш телефон">

                            </div>

                            <div class="input-box btn-box">

                                <button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Отправить
                                </button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>
        <!--  //////////////////////////////  -->

        <div class="popup-credit">

            <div class="popup-bg"></div>

            <div class="popup-credit-box">

                <div class="close-btn-box">

                    <button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
                    </button>

                </div>

                <div class="credit-box">

                    <div class="sect-h">

                        <h3 class="center-txt">Расчет</h3>

                        <h2 class="center-txt green-txt">кредита</h2>

                    </div>

                    <div class="credit-content">

                        <p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет
                            кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621"
                                                                             class="green-txt credit-tel-link">8 800
                                333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши
                            условия.</p>

                        <div class="credit-form offset-ziro">

                            <form>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Стоимость дома</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Cумма первоначального взноса</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Планируемый срок кредита</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">мес.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Сумма кредита</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row credit-btn-box">

                                    <button class="popup-credit-btn"><i class="fa fa-calculator" aria-hidden="true"></i>Рассчитать
                                    </button>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!--  Popup credit Tablet  -->

        <div class="popupcredit-tablet">

            <div class="popup-bg"></div>

            <div class="credit-tablet-box">

                <div class="credit-tablet-content">

                    <div class="close-btn-box">

                        <button class="close-popup-btn" type="button"><i
                                class="close-x-icon"></i><span>Закрыть окно</span></button>

                    </div>

                    <div class="popupcredit-tablet-scroll-box">

                        <div class="sect-h">

                            <h3 class="center-txt">Расчет</h3>

                            <h2 class="center-txt green-txt">кредита</h2>

                        </div>

                        <p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет
                            кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621"
                                                                             class="green-txt credit-tel-link">8 800
                                333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши
                            условия.</p>


                        <div class="credit-tablet-form offset-ziro">

                            <form>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Стоимость дома</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Cумма первоначального взноса</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Планируемый срок кредита</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">мес.</p>

                                    </div>

                                </div>

                                <div class="credit-row">

                                    <div class="credit-input-box focus-group focus">

                                        <div class="col col-1">

                                            <h3>Сумма кредита</h3>

                                            <input type="text" placeholder="999999">

                                        </div>

                                        <p class="rub col col-2">руб.</p>

                                    </div>

                                </div>

                                <div class="credit-row credit-btn-box">

                                    <button class="popup-credit-btn">Рассчитать</button>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!--  /Popup credit Tablet  -->

        <!--  //////////////////////////////  -->

        <div class="popup-credit-result">

            <div class="popup-bg"></div>

            <div class="popup-credit-result-box">

                <div class="close-btn-box">

                    <button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
                    </button>

                </div>

                <div class="credit-box">

                    <div class="sect-h">

                        <h3 class="center-txt">Расчет</h3>

                        <h2 class="center-txt green-txt">кредита</h2>

                    </div>

                    <!-- <div class="credit-content">

                        <p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621" class="green-txt credit-tel-link">8 800 333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши условия.</p>

                    </div> -->

                    <div class="sect-h">

                        <h3 class="center-txt">Итого</h3>

                    </div>

                    <div class="credit-content-last">

                        <p>При покупке дома стоимостью <span class="font-medium">1 248 800</span> с первоначальным <span
                                class="font-medium">взносом 800 000</span> минимальный ежемесячный платеж за кредит на
                            <span class="font-medium">36 месяцев</span> по ставке <span class="font-medium">14.5 процентов годовых</span>
                            составит:</p>

                    </div>

                    <div class="credite-price-box">

                        <p class="credit-price">15 448,13 <span class="credit-rub">р.</span></p>

                        <p class="bank-txt">по программе банка <a href="#" class="green-txt bank-link">Крокус Банк</a>
                        </p>

                    </div>

                    <div class="credit-row">

                        <button class="popup-credit-btn"><i class="fa fa-calculator" aria-hidden="true"></i>Новый расчет
                        </button>

                    </div>

                </div>

            </div>

        </div>


        <!--  Popup Credit Result Tablet  -->

        <div class="popup-credit-result-tablet">

            <div class="popup-bg"></div>

            <div class="popup-credit-result-tablet-box">

                <div class="close-btn-box">

                    <button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
                    </button>

                </div>

                <div class="credit-box credit-result-tablet-scroll">

                    <div class="sect-h">

                        <h3 class="center-txt">Расчет</h3>

                        <h2 class="center-txt green-txt">кредита</h2>

                    </div>

                    <div class="sect-h">

                        <h3 class="center-txt">Итого</h3>

                    </div>

                    <div class="credit-content-last">

                        <p>При покупке дома стоимостью <span class="font-medium">1 248 800</span> с первоначальным <span
                                class="font-medium">взносом 800 000</span> минимальный ежемесячный платеж за кредит на
                            <span class="font-medium">36 месяцев</span> по ставке <span class="font-medium">14.5 процентов годовых</span>
                            составит:</p>

                    </div>

                    <div class="credite-price-box">

                        <p class="credit-price">15 448,13 <span class="credit-rub">р.</span></p>

                        <p class="bank-txt">по программе банка <a href="#" class="green-txt bank-link">Крокус Банк</a>
                        </p>

                    </div>

                    <div class="credit-row">

                        <button class="popup-credit-btn">Новый расчет</button>

                    </div>

                </div>

            </div>

        </div>

        <!--  /Popup Credit Result Tablet  -->


        <!-- /Popus -->


        <section class="inner-page-sect-1 show-main-float-nav-coor">

            <div class="row">
                    <?php get_template_part('template-parts/parts/breadcrumbs'); // хлебные крошки ?>
                <div class="inner-h">

                    <h2><?= the_title() ?></h2>

                </div>

            </div>

        </section>

        <section class="slider-project-sect">

            <div class="row">

                <div class="project-slider-box slider_3 offset-ziro">

                    <div class="project-slider col col-1 project-slider-col">

                        <div class="project-big-slider">
                            <? foreach ($fields['photos'][0] as $photo) { ?>
                                <div><img src="<?= $photo['url'] ?>" alt=""></div>
                                <?
                            } ?>

                        </div>

                        <div class="project-miniatures-slider">
                            <? foreach ($fields['photos'][0] as $photo) { ?>
                                <div><img src="<?= $photo['sizes']['thumbnail'] ?>" alt=""></div>
                                <?
                            } ?>

                        </div>

                    </div>

                    <div class="project-info-box col col-2 project-slider-col">

                        <div class="project-info">

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-1"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Общая площадь</h3>
                                    <p><?= $fields['all_square']; ?> м<span class="sq-2">2</span></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-2"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Площадь террас(ы)</h3>

                                    <p><?= $fields['any_square']; ?> м<span class="sq-2">2</span></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-3"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество этажей</h3>

                                    <p><?= $fields['floors']; ?></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-4"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество спален</h3>

                                    <p><?= $fields['bedrooms']; ?></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-5"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество санузлов</h3>

                                    <p><?= $fields['bathrooms']; ?></p>

                                </div>

                            </div>

                            <p class="project-price"><?= number_format($fields['price'], 0, '.', ' '); ?> <span
                                    class="rub">Р.</span></p>

                            <div class="project-info-btn-box">

                                <button type="submit" class="send-btn call-callback"><i class="icon call-back-icon"
                                                                                        aria-hidden="true"></i>Отправить
                                    заявку
                                </button>

                                <button class="credit-btn"><i class="icon credit-icon"></i><span class="credit-link">Заказать кредит</span>
                                </button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section class="tabs-sect">

            <div class="row">

                <div class="tabs">

                    <div class="row">

                        <label for="tab1" class="card_tab_link active">Описание</label>
                        <label for="tab2" class="card_tab_link">Планировки</label>
                        <label for="tab3" class="card_tab_link">Состав работ</label>
                        <label for="tab4" class="card_tab_link">Гарантии</label>

                    </div>

                    <div class="tabs-content">

                        <input type="radio" name="tab" id="tab1">
                        <div id="tab1" class="card_tab">

                            <?= $fields['description']; ?>

                        </div>

                        <input type="radio" name="tab" id="tab2">
                        <div id="tab2" class="card_tab">
                            <?
                            foreach ($fields['plains'] as $plain) { ?>
                            <div class="plans-box">

                                <div class="plan col">

                                    <div class="plan-img-box">
                                        <img src="<?= $plain['plain_img']['url']; ?>" alt="">
                                    </div>

                                    <div class="plan-info">

                                        <div class="plan-info-header">

                                            <h3>Площадь <?= $plain['floor_value']; ?> этажа</h3>

                                            <p><?= $plain['floor_square']; ?> м<span class="sq">2</span></p>

                                        </div>

                                        <?
                                        if (!empty($plain['rooms_square'])) {
                                            foreach ($plain['rooms_square'] as $square) { ?>
                                                <div class="plan-info-table">

                                                    <div class="plan-info-table-row">

                                                        <div class="cell cell-1">

                                                            <h3><?= $square['rooms_name']; ?></h3>

                                                        </div>

                                                        <div class="cell cell-2">

                                                            <p><?= $square['rooms_square']; ?> м<span
                                                                    class="sq-2">2</span></p>

                                                        </div>

                                                    </div>

                                                </div>
                                            <? }
                                        }
                                        ?>
                                    </div>

                                </div>
                                <? } ?>


                            </div>


                        </div>

                        <input type="radio" name="tab" id="tab3">
                        <div id="tab3" class="card_tab">

                            <div class="tab-table two-cols">
                                <? foreach ($fields['job_description'] as $field) { ?>
                                    <div class="tab-col col-1">

                                        <div class="header-col">

                                            <h3>Комплектация</h3>

                                            <p><?= $field['equipment']; ?></p>

                                        </div>

                                        <div class="tab-txt">

                                            <h3>В состав комплекта входит:</h3>

                                            <ul class="with-mark">
                                                <? foreach ($field['estimate'] as $item) { ?>
                                                    <li><?= $item['name']; ?></li>
                                                    <?
                                                } ?>
                                            </ul>

                                            <h3 class="price-h">Стоимость:</h3>

                                            <p class="price-num"><?= number_format($fields['price']); ?> <span
                                                    class="rub">Р.</span></p>

                                        </div>

                                    </div>
                                <? }
                                ?>


                            </div>

                        </div>

                        <input type="radio" name="tab" id="tab4">
                        <div id="tab4" class="card_tab">

                            <?=$fields['guarantee'];?>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section class="project-thumbnails-1-sect">

            <div class="row">

                <div class="thumbnails project-thumbnails-1 offset-ziro">

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-1"></i>

                        </div>

                        <p class="p-main">Собственное производство</p>

                        <p class="p-2">клееного бруса</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-2"></i>

                        </div>

                        <p class="p-main">Бесплатное</p>

                        <p class="p-2">проектирование</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-3"></i>

                        </div>

                        <p class="p-main">качественная</p>

                        <p class="p-2">доставка и монтаж</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-4"></i>

                        </div>

                        <p class="p-main">гарантия 5 лет</p>

                        <p class="p-2">на все работы</p>

                    </div>

                </div>

            </div>

        </section>

        <section class="simmilar-offers-sect">

            <div class="simmilar-offers row">
                <h2>Схожие предложения</h2>
                <div class="simmilar-offers-thumbnails thumbnails three-cols offset-ziro for-resp-desc">

                    <?
                    $terms = get_categories(array(
                        'taxonomy' => 'proj_category'
                    ));
                    $args = array(
                        'post__not_in' => array(get_the_ID()),
                        'post_per_page' => 3,
                        'tax_query' => array(
                            array(
                                'taxonomy' => $terms[0]->taxonomy,
                                'terms' => $terms[0]->term_id
                            )
                        )
                    );

                    $query = new WP_Query;
                    $my_posts = $query->query($args);
                    $releted_posts = array();

                    foreach ($my_posts as $my_post) {
                        $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id($my_post->ID), array(375, 272));
                        $post_fields = get_fields($my_post->ID);

                        ?>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?= $thumbnail_attributes[0]; ?>" alt="">

                                    </div>

                                    <div class="mask">
                                        <a href="<?= the_permalink($my_post->ID); ?>" class="more-link"><i
                                                class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">


                                    <p class="p-1">Проект дома <?= get_the_title($my_post->ID) ?></p>

                                    <p class="p-2"><?= $post_fields['all_square']; ?> м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от
                                        <span><?= number_format($post_fields['price'], 0, '.', ' '); ?></span> <span
                                            class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <?
                    }
                    ?>

                </div>
                <!-- Уточнить что это вообще такое -->
                <div class="simmilar-offers-slider-box for-resp-mob">

                    <div class="simmilar-offers-slider slider-7 slider-slick-with-arrows">
                <?

                foreach ($my_posts as $my_post) {
                $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id($my_post->ID), array(375, 272));
                $post_fields = get_fields($my_post->ID);

                ?>
                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?=$thumbnail_attributes[0];?>" alt="">

                                </div>

                                <div class="mask">

                                    <a href="<?=get_the_permalink($my_post->ID)?>" class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома <?=get_the_title($my_post->ID)?></p>

                                <p class="p-2"><?=$post_fields['all_square']?> м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>><?= number_format($post_fields['price'], 0, '.', ' '); ?></span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                <?}?>

                    </div>

                    <div class="simmilar-offers-slider-arrows slick-append-arrows center-txt"></div>

                </div>

            </div>

        </section>
        <?
        if ($_COOKIE['viewedProd']){
        $latest_id = array_slice($_COOKIE['viewedProd'], 0, 4);
        ?>
        <section class="last-saw-sect">

            <div class="last-saw row">

                <h2>Просмотренные ранее</h2>
                <div class="last-saw-thumbnails thumbnails offset-ziro for-resp-desc">

                    <? foreach ($latest_id as $viewedProdId) {
                        $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id($viewedProdId), array(375, 272));
                        $post_fields = get_fields($viewedProdId);
                        ?>
                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?= $thumbnail_attributes[0]; ?>" alt="">

                                    </div>

                                    <div class="mask">

                                        <a href="<?= the_permalink($viewedProdId) ?>" class="more-link"><i
                                                class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>
                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома <?= get_the_title($viewedProdId) ?></p>

                                    <p class="p-2"><?= $post_fields['all_square']; ?> м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span><?= number_format($post_fields['price'], 0, '.', ' '); ?></span>
                                        <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>
                        <?

                    } ?>
                </div>
                <div class="last-saw-thumbnails-slider-box for-resp-mob">

                    <div class="last-saw-thumbnails-slider slider-8">
                        <? foreach ($latest_id as $viewedProdId) {
                        $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id($viewedProdId), array(375, 272));
                        $post_fields = get_fields($viewedProdId);
                        ?>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?=$thumbnail_attributes[0]; ?>/projects/photo_4.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a href="<?=get_the_permalink($viewedProdId)?>" class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома <?=get_the_title($viewedProdId)?></p>

                                    <p class="p-2">1<?=$post_fields['all_square'];?> м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span><?= number_format($post_fields['price'], 0, '.', ' '); ?></span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <?}?>


                    </div>

                    <div class="last-saw-thumbnails-slider-arrows center-txt"></div>

                </div>

                <? } ?>

            </div>

        </section>


        <section class="sect-10">

            <div class="row sect_10_block">

                <div class="sect-h">

                    <h3 class="white-txt">Закажите дом</h3>
                    <h2>Своей мечты</h2>

                </div>

                <p class="center-txt font-16">Оставьте вашу заявку и в ближайшее время мы составим персональный и
                    бесплатный проект на ваш дом.</p>

                <div class="send-order-form-box offset-ziro">

                    <form>

                        <div class="col col-1">

                            <div class="input-box input-trasparent">

                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                <input type="text" placeholder="Ваше имя">

                            </div>

                        </div>

                        <div class="col col-2">

                            <div class="input-box input-mob">

                                <i class="fa fa-mobile" aria-hidden="true"></i>
                                <input type="tel" placeholder="8 (921) 890-34-34">

                            </div>

                        </div>

                        <div class="col col-3">

                            <div class="input-btn-box">

                                <input type="button" value="отправить" class="input-btn-submit">

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </section>

    </div>
    <!-- /Content -->
    <script type="text/javascript">
        // ----------------------------------------------------------------

        $(".project-big-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            asNavFor: ".project-miniatures-slider"
        });

        $(".project-miniatures-slider").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".project-big-slider",
            arrows: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });

        // ----------------------------------------------------------------

        $(".slider-7").slick({
            slidesToShow: 1,
            slidesToScroll: 1
        });

        // ----------------------------------------------------------------

        $(".slider-8").slick({
            slidesToShow: 1,
            slidesToScroll: 1
        });

        // ----------------------------------------------------------------
    </script>
<?php
get_footer();
