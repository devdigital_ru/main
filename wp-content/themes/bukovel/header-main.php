<div class="wrapper main-page">


	<!-- Header -->

	<header>
		<div class="header">
				
			<div class="header-top-row offset-ziro">
				
				<div class="col col-1">

					<div class="main-nav-box">

						<div class="top-row">
						
							<a class="logo-box" href="/">
								
								<img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="">

							</a>

						</div>

						<ul class="main-nav-list">
							<?php 
								if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
									$menu_list = '';
									foreach ( (array) $menu_items as $key => $menu_item ) {
										$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
										$url = $menu_item->url; // URL ссылки
										$menu_list .= '<li><a class="main-nav-link" href="' . $url . '">' . $title . '</a></li>';
									}
									echo $menu_list;
								}
							?>
						</ul>

					</div>

				</div>

				<div class="col col-2">
					
					<ul class="soc-icons-list">
						
						<li><a class="soc-icon-link facebook" href="<?php the_field('facebook',42);?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
						<li><a class="soc-icon-link vk" href="<?php the_field('vk',42);?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
						<li><a class="soc-icon-link instagram" href="<?php the_field('instagram',42);?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

					</ul>

				</div>

				<div class="col col-3 select-box">
					
					<div class="select-menu">
						
						<select  class="select-1">
							
							<option>Белгород</option>
							<option>Воронеж</option>
							<option>Москва</option>
							<option>Санкт-Петербург</option>
							<option>Курск</option>
							<option>Липецк</option>

						</select>

					</div>

				</div>

				<div class="col col-4 contacts-header">

					<h3>многоканальный телефон</h3>

					<a class="head-tel-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

					<button type="button" class="call-back-btn"><i class="call-back-icon"></i>Заказать звонок</button>

				</div>					

			</div>

			<div class="main-nav-sect">
				
				<div class="for-desc">

					<div class="row offset-ziro">
					
						<div class="col col-1">
							
							<a href="/" class="main-nav-logo-box">
								
								<img src="<?php bloginfo('template_url'); ?>/img/logo_main_menu.png" alt="">

							</a>

						</div>

						<div class="col col-2">
							
							<ul class="float-nav">
								<?php 
								if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
									$menu_list = '';
									foreach ( (array) $menu_items as $key => $menu_item ) {
										$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
										$url = $menu_item->url; // URL ссылки
										$menu_list .= '<li><a class="nav-link" href="' . $url . '">' . $title . '</a></li>';
									}
									echo $menu_list;
									}
								?>

							</ul>

						</div>

						<div class="col col-3 for-desc-nav-contacts">
							
							<!-- <button class="float-call-back-btn"><i class="float-call-back-icon"></i></button> -->

							<a class="head-tel-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

							<button type="button" class="call-back-btn"><i class="call-back-icon"></i>Заказать звонок</button>

						</div>

					</div>

				</div>

				<div class="for-tab for-tab-nav">

					<div class="for-tab-nav-bg">

						<div class="row offset-ziro">
						
							<div class="col col-1 respmenubtn-box">
								
								<button class="respmenubtn">
									
									<span></span>
									<span></span>
									<span></span>

								</button>

							</div>

							<div class="col col-2">
								
								<a class="resp-logo" href="/">

									<div class="resp-logo-img-box">

										<img src="<?php bloginfo('template_url'); ?>/img/logo_main_menu.png" alt="">

									</div>

									<p>Буковель<span class="green-txt">Дом</span></p>

								</a>

							</div>

							<div class="col col-3 clearfix">
								
								<button class="phone-resp-btn right"></button>

							</div>

						</div>

					</div>

					<div class="tab-navigation">
						
						<ul class="tab-nav">
							<?php 
								if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
									$menu_list = '';
									foreach ( (array) $menu_items as $key => $menu_item ) {
										$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
										$url = $menu_item->url; // URL ссылки
										$menu_list .= '<li><a class="tab-nav-link" href="' . $url . '">' . $title . '</a></li>';
									}
									echo $menu_list;
									}
								?>

						</ul>

						<div class="contact-resp-nav">
							
							<h3>Звонок по России бесплатный</h3>

							<a class="contact-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

						</div>

					</div>

				</div>

			</div>

		</div>

								
	</header>

	<!-- /Header -->

