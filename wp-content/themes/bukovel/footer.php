<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bukovel
 */

?>

			<!-- Footer -->

		<footer>
			
			<div class="footer">
					
					<div class="footer-row-1 four-cols offset-ziro">

						<div class="row">
							
							<div class="col col-1">
								
								<h2 class="slide-btn" id="slide_block_3" >Проекты</h2><i class="fa fa-sort-asc" aria-hidden="true"></i>
								<h2 class="slide-btn" id="slide_block_3" >Проекты</h2><i class="fa fa-sort-asc" aria-hidden="true"></i>

								<div class="slide-block-wrapp" id="drop_slide_block_3">

									<div class="slide-block accordion-dropdown">

										<ul class="seo-text">								

											<li><a class="footer-link" href="#">Дома</a></li>
											<li><a class="footer-link" href="#">Беседки</a></li>
											<li><a class="footer-link" href="#">Комплексы</a></li>
											<li><a class="footer-link" href="#">Бани</a></li>
											<li><a class="footer-link" href="#">Цены на Брус</a></li>
											<li><a class="footer-link" href="#">Коттеджи</a></li>
											<li><a class="footer-link" href="#">Гостевые дома</a></li>

										</ul>

									</div>

								</div>

							</div>

							<div class="col col-2">
								
								<h2 class="slide-btn" id="slide_block_4">Услуги</h2><i class="fa fa-sort-asc" aria-hidden="true"></i>

								<div class="slide-block-wrapp" id="drop_slide_block_4">

									<div class="slide-block accordion-dropdown">

										<ul class="seo-text">

											<li><a class="footer-link" href="#">Проектирование</a></li>
											<li><a class="footer-link" href="#">Строительство домов</a></li>
											<li><a class="footer-link" href="#">Фундамент</a></li>
											<li><a class="footer-link" href="#">Стены</a></li>
											<li><a class="footer-link" href="#">Кровля</a></li>
											<li><a class="footer-link" href="#">Коммуникации</a></li>
											<li><a class="footer-link" href="#">Интерьеры</a></li>

										</ul>

									</div>

								</div>

							</div>

							<div class="col col-3">
								
								<h2 class="slide-btn" id="slide_block_5">Буковельдом</h2><i class="fa fa-sort-asc" aria-hidden="true"></i>

								<div class="slide-block-wrapp" id="drop_slide_block_5">

									<div class="slide-block accordion-dropdown">

										<ul class="seo-text">

											<li><a class="footer-link" href="#">О компании</a></li>
											<li><a class="footer-link" href="#">Процесс производства</a></li>
											<li><a class="footer-link" href="#">Статьи</a></li>
											<li><a class="footer-link" href="#">Партнеры</a></li>
											<li><a class="footer-link" href="#">Контакты</a></li>
											<li><a class="footer-link" href="#">Публикации в СМИ</a></li>
										
										</ul>

									</div>

								</div>

							</div>

							<div class="col col-4">
								
									<h2 class="slide-btn" id="slide_block_6">Контакты</h2><i class="fa fa-sort-asc" aria-hidden="true"></i>

									<div class="slide-block-wrapp" id="drop_slide_block_6">

										<div class="slide-block accordion-dropdown">

											<div class="contant-box">
												
												<h4>Центральный офис</h4>

												<a href="tel:88003339621" class="tel-link"><span class="green">8 800</span> 333-96-21</a>

												<a class="mail-link" href="mailto:office@bukoveldom.ru">office@bukoveldom.ru</a>

											</div>

											<div class="contant-box">
												
												<h4>Санкт-Петебург</h4>

												<a href="tel:88124263509" class="tel-link"><span class="green">8 (812)</span> 426-35-09</a>

												<p class="map-p"><i class="fa fa-map-marker" aria-hidden="true"></i>Петроградский р-н, ул. Шамшева 10</p>


											</div>

										</div>

									</div>

							</div>

						</div>

					</div>

					<div class="footer-row-bottom offset-ziro">

						<div class="row">
							
							<div class="col col-1">

								<ul class="soc-link-list for-resp-tab">
									
									<li><a class="soc-link" href="<?php the_field('twitter',42);?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('facebook',42);?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('vk',42);?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('youtube',42);?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

								</ul>
								
								<p>&copy; 2010  —  2016, БуковельДом.</p>

								<p class="grey">Производство домов из клееного бруса</p>

							</div>

							<div class="col col-2">
								
								<ul class="soc-link-list">
									
									<li><a class="soc-link" href="<?php the_field('twitter',42);?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('facebook',42);?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('vk',42);?>"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
									<li><a class="soc-link" href="<?php the_field('youtube',42);?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>

								</ul>

							</div>

							<div class="col col-3 corp-box">

								<p>Разработка дизайна  —  <a class="green" href="#">Вадим Прусаков</a></p>

								<p>Разработка сайта и поддержка  —  <a class="green" href="#">INGATE</a></p>

							</div>

						</div>

					</div>

			</div>

		</footer>


       

		<!-- /Footer -->

		<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>

		<script>

			(function($){

				$(window).on("load",function(){
					
					$(".tab-navigation").mCustomScrollbar();

				    $(".callback-tablet-box").mCustomScrollbar();

				    $(".sidebar-with-scroll").mCustomScrollbar();

				    $(".credit-box").mCustomScrollbar();

				});

			})(jQuery);

			// --------------------------

			$(".slider_1").slick({
				dots: true,
				arrows: false,
				speed: 900,
				fade: true,
				slidesToShow: 1,
				appendDots: $(".slick-dots-append"),
				responsive: [
	  			  	{
				      breakpoint: 768,
				      settings: {
				      	appendDots: $(".slick-dots-append")
				      }
				    }
				  ]
			});

			$('.slider_1').on('afterChange', function(event, slick, currentSlide, nextSlide){

			  $(".slide-info.change-content").html( $(".slick-slide:eq("+ currentSlide +") .slide-info").html() ) ;

			  $(".for-resp-slideinfo .morelink-slide").offset({ top: ( $(".slider-main").height() - $(".for-resp-slideinfo .morelink-slide").outerHeight(true) - 15 ) });			  

			});

			// --------------------------

			$(".our-projects-slider").slick({				
				dots: false,
				arrows: true,
				slidesToShow: 1
			});

			// --------------------------

			$(".our-objects-slider").slick({
				dots: false,
				arrows: true,
				slidesToShow: 1
			});

			// --------------------------

			$(".slider_2").slick({
				dots: true,
				arrows: false,
				speed: 900,
				autoplaySpeed: 7000,
				slidesToShow: 2,
				responsive: [
	  			  	{
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 1,
				        dots: false
				      }
				    }
				  ]		
			});

			$(".leftSlickArrow").on("click", function(){
			    $(".slider_2").slick("slickPrev");
			});

			$(".rightSlickArrow").on("click", function(){
			    $(".slider_2").slick("slickNext");
			});

			// -------------------------
			
			$("select").select2();
			
		</script>
	
	</div>


<!--  Popus  -->







<div class="popup-calback">

	<div class="popup-bg"></div>

	<div class="callback-box">

		<div class="close-btn-box">

			<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
			</button>

		</div>

		<div class="callback-content">

			<div class="sect-h">

				<h3 class="center-txt">заказ</h3>

				<h2 class="center-txt green-txt">звонка</h2>

			</div>

			<p>Оставьте ваши контактные данные, и наши менеджеры свяжутся с вами в ближайшее время, чтобы
				обсудить все детали.</p>

			<div class="callback-form">


				<?= do_shortcode('[contact-form-7 id="70" title="Форма в попапе"]'); ?>


			</div>

		</div>

	</div>

</div>

<!--  Popup calback Tablet  -->

<div class="popup-calback-tablet">

	<div class="popup-bg"></div>

	<div class="callback-tablet-box">

		<div class="callback-tablet-content">

			<div class="callback-tablet-h">

				<h3>Звонок по России бесплатный</h3>

				<a href="tel:88003339621" class="callback-tablet-tel"><span class="green-txt">8 800</span>
					333-96-21</a>

			</div>

			<p>Оставьте вашу заявку и в ближайшее время мы составим персональный и бесплатный проект на ваш
				дом</p>

			<div class="callback-form">


				<?= do_shortcode('[contact-form-7 id="70" title="Форма в попапе"]'); ?>


			</div>

		</div>

	</div>

</div>



<!--  //////////////////////////////  -->

<div class="popup-credit">

	<div class="popup-bg"></div>

	<div class="popup-credit-box">

		<div class="close-btn-box">

			<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
			</button>

		</div>

		<div class="credit-box">

			<div class="sect-h">

				<h3 class="center-txt">Расчет</h3>

				<h2 class="center-txt green-txt">кредита</h2>

			</div>

			<div class="credit-content">

				<p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет
					кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621"
																	 class="green-txt credit-tel-link">8 800
						333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши
					условия.</p>

				<div class="credit-form offset-ziro">

					<form>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Стоимость дома</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Cумма первоначального взноса</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Планируемый срок кредита</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">мес.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Сумма кредита</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row credit-btn-box">

							<button class="popup-credit-btn"><i class="fa fa-calculator" aria-hidden="true"></i>Рассчитать
							</button>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>

</div>

<!--  Popup credit Tablet  -->

<div class="popupcredit-tablet">

	<div class="popup-bg"></div>

	<div class="credit-tablet-box">

		<div class="credit-tablet-content">

			<div class="close-btn-box">

				<button class="close-popup-btn" type="button"><i
						class="close-x-icon"></i><span>Закрыть окно</span></button>

			</div>

			<div class="popupcredit-tablet-scroll-box">

				<div class="sect-h">

					<h3 class="center-txt">Расчет</h3>

					<h2 class="center-txt green-txt">кредита</h2>

				</div>

				<p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет
					кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621"
																	 class="green-txt credit-tel-link">8 800
						333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши
					условия.</p>


				<div class="credit-tablet-form offset-ziro">

					<form>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Стоимость дома</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Cумма первоначального взноса</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Планируемый срок кредита</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">мес.</p>

							</div>

						</div>

						<div class="credit-row">

							<div class="credit-input-box focus-group focus">

								<div class="col col-1">

									<h3>Сумма кредита</h3>

									<input type="text" placeholder="999999">

								</div>

								<p class="rub col col-2">руб.</p>

							</div>

						</div>

						<div class="credit-row credit-btn-box">

							<button class="popup-credit-btn">Рассчитать</button>

						</div>

					</form>

				</div>

			</div>

		</div>

	</div>

</div>

<!--  /Popup credit Tablet  -->

<!--  //////////////////////////////  -->

<div class="popup-credit-result">

	<div class="popup-bg"></div>

	<div class="popup-credit-result-box">

		<div class="close-btn-box">

			<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
			</button>

		</div>

		<div class="credit-box">

			<div class="sect-h">

				<h3 class="center-txt">Расчет</h3>

				<h2 class="center-txt green-txt">кредита</h2>

			</div>

			<!-- <div class="credit-content">

                <p>В данном окне вы можете сделать <span class="uppercase">предварительный</span> расчет кредита. Для уточнения, позвоните по телефону <a href="tel:88003339621" class="green-txt credit-tel-link">8 800 333-96-21</a> и наши менеджеры сделают вам более точный кредитный расчет под ваши условия.</p>

            </div> -->

			<div class="sect-h">

				<h3 class="center-txt">Итого</h3>

			</div>

			<div class="credit-content-last">

				<p>При покупке дома стоимостью <span class="font-medium">1 248 800</span> с первоначальным <span
						class="font-medium">взносом 800 000</span> минимальный ежемесячный платеж за кредит на
					<span class="font-medium">36 месяцев</span> по ставке <span class="font-medium">14.5 процентов годовых</span>
					составит:</p>

			</div>

			<div class="credite-price-box">

				<p class="credit-price">15 448,13 <span class="credit-rub">р.</span></p>

				<p class="bank-txt">по программе банка <a href="#" class="green-txt bank-link">Крокус Банк</a>
				</p>

			</div>

			<div class="credit-row">

				<button class="popup-credit-btn"><i class="fa fa-calculator" aria-hidden="true"></i>Новый расчет
				</button>

			</div>

		</div>

	</div>

</div>


<!--  Popup Credit Result Tablet  -->

<div class="popup-credit-result-tablet">

	<div class="popup-bg"></div>

	<div class="popup-credit-result-tablet-box">

		<div class="close-btn-box">

			<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
			</button>

		</div>

		<div class="credit-box credit-result-tablet-scroll">

			<div class="sect-h">

				<h3 class="center-txt">Расчет</h3>

				<h2 class="center-txt green-txt">кредита</h2>

			</div>

			<div class="sect-h">

				<h3 class="center-txt">Итого</h3>

			</div>

			<div class="credit-content-last">

				<p>При покупке дома стоимостью <span class="font-medium">1 248 800</span> с первоначальным <span
						class="font-medium">взносом 800 000</span> минимальный ежемесячный платеж за кредит на
					<span class="font-medium">36 месяцев</span> по ставке <span class="font-medium">14.5 процентов годовых</span>
					составит:</p>

			</div>

			<div class="credite-price-box">

				<p class="credit-price">15 448,13 <span class="credit-rub">р.</span></p>

				<p class="bank-txt">по программе банка <a href="#" class="green-txt bank-link">Крокус Банк</a>
				</p>

			</div>

			<div class="credit-row">

				<button class="popup-credit-btn">Новый расчет</button>

			</div>

		</div>

	</div>

</div>

<!--  /Popup Credit Result Tablet  -->

<!--  //////////////////////////////  -->

<div class="popup-video">

	<div class="popup-bg"></div>

	<div class="popup-video-box">

		<div class="close-btn-box">

			<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span>
			</button>

		</div>

		<div class="video-box">

			<iframe width="560" height="315" src="<?= get_field('video_link', 42); ?>" allowfullscreen
					id="video"></iframe>

		</div>

	</div>

</div>

<!-- /Popus -->


<?php wp_footer(); ?>

</body>
</html>
