<?php
	global $post;
	$thePostID = $post->ID;
?>
<div class="wrapper">

	<!-- Header page-->

	<header>
		<div class="header-inner-page">

			<div class="header-inner-top-row offset-ziro">

				<div class="row">

					<div class="col col-1">

						<a href="/" class="logo-box-inner-page">

							<div class="logo-img-inner-page col-1-col">
							
								<img src="<?php bloginfo('template_url'); ?>/img/logo_inner_page.png" alt="">

							</div>

							<div class="logo-col-txt col-1-col">
								
								<h1 class="main-h">Буковель<span class="green-txt">дом</span></h1>

							</div>

						</a>

					</div>

					<div class="col col-2 select-box">
						
						<div class="select-menu">
								
							<select  class="select-1">
								
								<option>Белгород</option>
								<option>Воронеж</option>
								<option>Москва</option>
								<option>Санкт-Петербург</option>
								<option>Курск</option>
								<option>Липецк</option>

							</select>

						</div>

					</div>

					<div class="col col-3 contacts-header">

						<h3>Звонок по России бесплатный</h3>
						
						<a class="head-tel-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

					</div>

					<div class="col col-4 contacts-header">

						<button type="button" class="call-back-btn"><i class="call-back-icon"></i>Заказать звонок</button>	

					</div>

				</div>

			</div>

			<div class="inner-page-nav-box">

				<div class="inner-page-nav">

					<ul class="inner-nav">
						<?php 
							if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
								$menu_list = '';
								foreach ( (array) $menu_items as $key => $menu_item ) {
									$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
									// echo "<pre>";
									// var_dump($menu_item);
									// echo "</pre>";
									$url = $menu_item->url; // URL ссылки
									if($thePostID == $menu_item->object_id) {
									        $menu_list .= '<li><a class="nav-link active" href="' . $url . '">' . $title . '</a></li>';
									    }else{
									        $menu_list .= '<li><a class="nav-link" href="' . $url . '">' . $title . '</a></li>';
							    	}
									
								}
								echo $menu_list;
							}

						?>
					</ul>

				</div>

			</div>

			<div class="main-nav-sect">
				
				<div class="for-desc">

					<div class="row offset-ziro">
					
						<div class="col col-1">
							
							<a href="/" class="main-nav-logo-box">
								
								<img src="<?php bloginfo('template_url'); ?>/img/logo_main_menu.png" alt="">

							</a>

						</div>

						<div class="col col-2">
							
							<ul class="float-nav">
								<?php 
									if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
										$menu_list = '';
										foreach ( (array) $menu_items as $key => $menu_item ) {
											$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
											// echo "<pre>";
											// var_dump($menu_item);
											// echo "</pre>";
											$url = $menu_item->url; // URL ссылки
											if($thePostID == $menu_item->object_id) {
											        $menu_list .= '<li><a class="nav-link active" href="' . $url . '">' . $title . '</a></li>';
											    }else{
											        $menu_list .= '<li><a class="nav-link" href="' . $url . '">' . $title . '</a></li>';
									    	}
											
										}
										echo $menu_list;
									}

								?>
							</ul>

						</div>

						<div class="col col-3 for-desc-nav-contacts">

							<a class="head-tel-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

							<button type="button" class="call-back-btn"><i class="call-back-icon"></i>Заказать звонок</button>

						</div>

					</div>

				</div>

				<div class="for-tab for-tab-nav">

					<div class="for-tab-nav-bg">

						<div class="row offset-ziro">
						
							<div class="col col-1 respmenubtn-box">
								
								<button class="respmenubtn">
									
									<span></span>
									<span></span>
									<span></span>

								</button>

							</div>

							<div class="col col-2">
								
								<a class="resp-logo" href="/">

									<div class="resp-logo-img-box">

										<img src="<?php bloginfo('template_url'); ?>/img/logo_main_menu.png" alt="">

									</div>

									<p>Буковель<span class="green-txt">Дом</span></p>

								</a>

							</div>

							<div class="col col-3 clearfix">
								
								<button class="phone-resp-btn right">
									
									<i class="icon phone-resp-btn-icon"></i>

								</button>

							</div>

						</div>

					</div>

					<div class="tab-navigation">
						
						<ul class="tab-nav">

							<li><a href="/" class="tab-nav-link">Главная</a></li>
							<?php 
								if( $menu_items = wp_get_nav_menu_items('Главное меню') ) { // "Меню для шапки" - это название моего меню. Вы можете также использовать ID или ярлык
									$menu_list = '';
									foreach ( (array) $menu_items as $key => $menu_item ) {
										$title = $menu_item->title; // заголовок элемента меню (анкор ссылки)
										// echo "<pre>";
										// var_dump($menu_item);
										// echo "</pre>";
										$url = $menu_item->url; // URL ссылки
										if($thePostID == $menu_item->object_id) {
										        $menu_list .= '<li><a class="tab-nav-link active" href="' . $url . '">' . $title . '</a></li>';
										    }else{
										        $menu_list .= '<li><a class="tab-nav-link" href="' . $url . '">' . $title . '</a></li>';
								    	}
										
									}
									echo $menu_list;
								}

							?>

						</ul>

						<div class="contact-resp-nav">
							
							<h3>Звонок по России бесплатный</h3>

							<a class="contact-link" href="tel:88003339621"><span class="green">8 800</span> 333-96-21</a>

						</div>

					</div>

				</div>

			</div>

		</div>

								
	</header>

	<!-- /Header -->
