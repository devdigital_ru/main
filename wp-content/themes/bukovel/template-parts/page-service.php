<?php

/* Template Name: Шаблон Услуги*/


get_header(); ?>

<!-- Content -->
	<div class="content">

		<!--  Popus  -->

		<div class="popup-calback">
			
			<div class="popup-bg"></div>

			<div class="callback-box">

				<div class="close-btn-box">

					<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span></button>

				</div>		

				<div class="callback-content">

					<div class="sect-h">
				
						<h3 class="center-txt">заказ</h3>

						<h2 class="center-txt green-txt">звонка</h2>

					</div>

					<p>Оставьте ваши контактные данные, и наши менеджеры свяжутся с вами в ближайшее время, чтобы обсудить все детали.</p>

					<div class="callback-form">

						<form>

							<div class="input-box">
								
								<i class="fa fa-user-o" aria-hidden="true"></i>
								<input type="text" placeholder="Ваше имя">

							</div>

							<div class="input-box">
								
								<i class="fa fa-mobile" aria-hidden="true"></i>
								<input type="tel" placeholder="Ваш телефон">

							</div>

							<div class="input-box btn-box">
								
								<button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Отправить</button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>

		<!--  Popup calback Tablet  -->

		<div class="popup-calback-tablet">
			
			<div class="popup-bg"></div>

			<div class="callback-tablet-box">

				<div class="callback-tablet-content">

					<div class="callback-tablet-h">

						<h3>Звонок по России бесплатный</h3>

						<a href="tel:88003339621" class="callback-tablet-tel"><span class="green-txt">8 800</span> 333-96-21</a>

					</div>

					<p>Оставьте вашу заявку и в ближайшее время мы составим персональный и бесплатный проект на ваш дом</p>

					<div class="callback-form">

						<form>

							<div class="input-box">
								
								<i class="fa fa-user-o" aria-hidden="true"></i>
								<input type="text" placeholder="Ваше имя">

							</div>

							<div class="input-box">
								
								<i class="fa fa-mobile" aria-hidden="true"></i>
								<input type="tel" placeholder="Ваш телефон">

							</div>

							<div class="input-box btn-box">
								
								<button type="submit">Отправить</button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>

		<!--  /Popup calback Tablet  -->

		<!--  //////////////////////////////  -->

		<div class="popup-video">
			
			<div class="popup-bg"></div>

			<div class="popup-video-box">

				<div class="close-btn-box">

					<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span></button>

				</div>

				<div class="video-box" id="video-placeholder">
					
					<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/Re2hxifRCiY" frameborder="0" allowfullscreen id="video"></iframe> -->

				</div>

			</div>

		</div>

		<!-- /Popus -->



		<section class="inner-page-sect-1 show-main-float-nav-coor">
			
			<div class="row">
				
				<div class="breadcrumbs">
					
					<ul class="breadcrumbs-list">
						
						<li><a class="breadcrumb-link" href="#">Главная</a></li>

						<li><a class="breadcrumb-link" href="#">Услуги</a></li>

					</ul>

				</div>

				<div class="inner-h">
					
					<h2>Услуги</h2>

				</div>

			</div>

		</section>

		<section class="services-thumbnail-sect">
			
			<div class="row">
				
				<h2>Мы предлагаем следующие услуги:</h2>

				<div class="services thumbnails offset-ziro">
					
					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-1"></i>

						<h3>Проектирование</h3>

						<p>Мы предлагаем профессиональное проектирование деревянных домов, которые рассчитаны на века верной службы.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-2"></i>

						<h3>Строительство</h3>

						<p>Весь процесс любой стройки всегда контролируется от начала и до конца руководством <span class="green-txt">BukovelDom</span>, будь то даже удаленные объекты</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-3"></i>

						<h3>Фундамент</h3>

						<p>Начало строительства любого объекта – это, в первую очередь, фундамент. И именно с него начинается создание основы для Вашего будущего дома.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-4"></i>

						<h3>Стены</h3>

						<p>Возведение стен из клееного бруса «<span class="green-txt">BukovelDom</span>» – наиболее интересный и необычный для клиента процесс.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-5"></i>

						<h3>Кровля</h3>

						<p>Хорошая крыша над головой - основа вашего спокойствия и удобства нового дома. Мы знаем все, как сделать кровлю профессионально и качественно.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-6"></i>

						<h3>Коммуникации</h3>

						<p>Используя весь арсенал современных технологий, мы оснащаем дома инженерными системами, отвечающими мировым стандартам и Вашим пожеланиям.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-7"></i>

						<h3>Интерьеры</h3>

						<p>По Вашему желанию на любом этапе ведения проекта мы готовы привлечь лучших  специалистов для разработки эксклюзивных дизайнов внутренних интерьеров.</p>

					</a>

					<a href="#" class="thumbnail">
						
						<i class="service-icon icon-8"></i>

						<h3>Типовые дома</h3>

						<p>Весь процесс любой стройки всегда контролируется от начала и до конца руководством <span class="green-txt">BukovelDom</span>, будь то даже удаленные объекты.</p>

					</a>

				</div>

			</div>

		</section>

		

	</div>
<!-- /Content -->
<?php
get_footer();
