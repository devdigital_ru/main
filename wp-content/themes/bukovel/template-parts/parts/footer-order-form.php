<section class="sect-10">

    <div class="row sect_10_block">

        <div class="sect-h">

            <h3 class="white-txt">Закажите дом</h3>
            <h2>Своей мечты</h2>

        </div>

        <p class="center-txt font-16">Оставьте вашу заявку и в ближайшее время мы составим персональный и
            бесплатный проект на ваш дом.</p>

        <div class="send-order-form-box offset-ziro">


            <?=do_shortcode('[contact-form-7 id="102" title="Закажите дом своей мечты"]');?>



        </div>

    </div>

</section>