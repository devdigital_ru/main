<section class="sect-7">

					<div class="row portfolio map-section">

						<div class="map-content" id="map">



							<div class="map-object-info">

								<button type="button" class="close-map-object-info"><i class="close-x-icon"></i></button>
								<!-- <button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span></button> -->

								<div class="object-info-append">

									<a href="#" class="object-info">

										<div class="object-photo-box">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

										</div>

										<div class="map-object-info-content">

											<h3>Коттеджный поселок</h3>

											<h3>«Никольское-Лесное»</h3>

											<p>Ленинградское шоссе, 26 км</p>

										</div>

									</a>

								</div>

							</div>

							<button class="show-map-btn"><i class="fa fa-map-marker" aria-hidden="true"></i><span class="map-btn-txt">Раскрыть карту объектов</span></button>

						</div>

						<div class="map-box">

							<img class="map-img" src="<?php bloginfo('template_url'); ?>/img/map.jpg" alt="">

							<div class="marker-box marker-1">

								<i class="map-marker-icon"></i>

								<div class="object-info-for-append">

									<a href="" class="object-info">

										<div class="object-photo-box">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

										</div>

										<div class="map-object-info-content">

											<h3>Коттеджный поселок</h3>

											<h3>«Никольское-Лесное»</h3>

											<p>Ленинградское шоссе, 26 км</p>

										</div>

									</a>

								</div>

							</div>

							<div class="marker-box marker-2">

								<i class="map-marker-icon"></i>

								<div class="object-info-for-append">

									<a href="#" class="object-info">

										<div class="object-photo-box">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

										</div>

										<div class="map-object-info-content">

											<h3>Объект 2</h3>

											<h3>«Никольское-Лесное»</h3>

											<p>Ленинградское шоссе, 26 км</p>

										</div>

									</a>

								</div>

							</div>

							<div class="marker-box marker-3">

								<i class="map-marker-icon"></i>

								<div class="object-info-for-append">

									<a href="#" class="object-info">

										<div class="object-photo-box">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

										</div>

										<div class="map-object-info-content">

											<h3>Объект 3</h3>

											<h3>«Никольское-Лесное»</h3>

											<p>Ленинградское шоссе, 26 км</p>

										</div>

									</a>

								</div>

							</div>

						</div>


					</div>

				</section>