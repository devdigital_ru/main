<?php

/* Template Name: Шаблон акции*/


get_header(); ?>
	<!-- Content -->
	<div class="content">

		<!--  Popus  -->

		<div class="popup-calback">
			
			<div class="popup-bg"></div>

			<div class="callback-box">

				<div class="close-btn-box">

					<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span></button>

				</div>		

				<div class="callback-content">

					<div class="sect-h">
				
						<h3 class="center-txt">заказ</h3>

						<h2 class="center-txt green-txt">звонка</h2>

					</div>

					<p>Оставьте ваши контактные данные, и наши менеджеры свяжутся с вами в ближайшее время, чтобы обсудить все детали.</p>

					<div class="callback-form">

						<form>

							<div class="input-box">
								
								<i class="fa fa-user-o" aria-hidden="true"></i>
								<input type="text" placeholder="Ваше имя">

							</div>

							<div class="input-box">
								
								<i class="fa fa-mobile" aria-hidden="true"></i>
								<input type="tel" placeholder="Ваш телефон">

							</div>

							<div class="input-box btn-box">
								
								<button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Отправить</button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>

		<!--  Popup calback Tablet  -->

		<div class="popup-calback-tablet">
			
			<div class="popup-bg"></div>

			<div class="callback-tablet-box">

				<div class="callback-tablet-content">

					<div class="callback-tablet-h">

						<h3>Звонок по России бесплатный</h3>

						<a href="tel:88003339621" class="callback-tablet-tel"><span class="green-txt">8 800</span> 333-96-21</a>

					</div>

					<p>Оставьте вашу заявку и в ближайшее время мы составим персональный и бесплатный проект на ваш дом</p>

					<div class="callback-form">

						<form>

							<div class="input-box">
								
								<i class="fa fa-user-o" aria-hidden="true"></i>
								<input type="text" placeholder="Ваше имя">

							</div>

							<div class="input-box">
								
								<i class="fa fa-mobile" aria-hidden="true"></i>
								<input type="tel" placeholder="Ваш телефон">

							</div>

							<div class="input-box btn-box">
								
								<button type="submit">Отправить</button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>

		<!--  /Popup calback Tablet  -->

		<!--  //////////////////////////////  -->

		<section class="inner-page-sect-1 show-main-float-nav-coor">
			
			<div class="row">
				
				<div class="breadcrumbs">
					
					<ul class="breadcrumbs-list">
						
						<li><a class="breadcrumb-link" href="/">Главная</a></li>
						<li><a class="breadcrumb-link" href="/o-kompanii/">О компании</a></li>

						<li><a class="breadcrumb-link" href="javascript:void(0)">Акции</a></li>

					</ul>

				</div>

				<div class="inner-h">
					
					<h2>Акции</h2>

				</div>

			</div>

		</section>
		<!--promo main-->
		<section class="promo_main">
			<div class="row">
				<?php
			        $args_big = array(
			            'post_type' => 'promo',
			            'posts_per_page' => -1
			        );
			        // Запрос. $args - параметры запроса
			        query_posts( $args_big );
			        // Цикл WordPress
		            if(have_posts() ):
		                while( have_posts() ) : the_post();?>
		            		<div class="article_promo-wrap" id="promo_<?php echo get_the_ID();?>">
								<div class="article_promo-img">
									<?php the_post_thumbnail();?>
									<h2><?php the_field('заголовок_1');?> <?php the_field('заголовок_2');?></h2>
								</div>
								<h3 class="article_promo-title"><?php the_title();?></h3>
								<p class="article_promo-date">Срок проведения – c <span><?php the_field('дата_акции_начало');?></span> - <span><?php the_field('дата_акции_конец');?></span></p>
								<p class="article_promo-txt"><?php the_content();?></p>
								<button class="all-projects article_promo-btn"><i></i>Оставить заявку</button>
							</div>
		               	<?php endwhile;?>
		                <?php wp_reset_query(); ?>
					<?php endif;?>
				
			</div>
		</section>
	</div>
<?php
get_footer();
