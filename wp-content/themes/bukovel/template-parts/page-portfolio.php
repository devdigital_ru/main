<?php

/* Template Name: Шаблон Портфолио*/


get_header(); ?>
    <!-- Content -->
    <div class="content">


        <section class="inner-page-sect-1 show-main-float-nav-coor">

            <div class="row">

                <?php get_template_part('template-parts/parts/breadcrumbs'); // хлебные крошки ?>

                <div class="inner-h">

                    <h2><? the_title(); ?></h2>

                </div>

            </div>

        </section>


        <section class="filter-section">


            <? // собираем информацию
            $obj_type_arr = array(); // пустой массив
            $obj_status_arr = array(); //  пустой массив

            // параметры
            $args = array(
                'post_type' => 'objects',
                'posts_per_page' => -1,

                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'obj_type',
                        'value' => $obj_type_arr,
                        'compare' => 'IN',
                    ),
                    array(
                        'key' => 'статус_готовности_объекта',
                        'value' => $obj_status_arr,
                        'compare' => 'IN'
                    )
                ),
            );

            $posts_before = query_posts(array(
                'post_type' => 'objects',
                'posts_per_page' => -1,

            ));



            $posts = query_posts($args);
            $count = count($posts); // количество постов по заданному $args


            $i = 0;
            foreach ($posts_before as $pt) {

                $obj_type = get_field('obj_type', $pt->ID);
                $obj_status = get_field('статус_готовности_объекта', $pt->ID);

                $obj_type_arr[$i] = $obj_type;
                $obj_status_arr[$i] = $obj_status;


                $i++;

            }

            $obj_type_arr = array_unique($obj_type_arr);
            $obj_status_arr = array_unique($obj_status_arr);


            function debug_to_console( $data ) {

                if ( is_array( $data ) )
                    $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
                else
                    $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

                echo $output;
            }



            ?>


            <div class="row filter">

                <div class="filter-h-box">

                    <h2 class="filter-h">Фильтр</h2>

                </div>

                <div class="filter-box">

                    <div class="filters-wrapp">

                        <div class="filters-content">

                            <form action="" method="get" id="portfolio_filter_form">

                                <div class="filter-row">

                                    <div class="col col-1">

                                        <h3>Тип объекта</h3>

                                    </div>

                                    <div class="col col-2">

                                        <?
                                        $index = 1;
                                        foreach ($obj_type_arr as $value): ?>

                                        <div class="checkbox-box">
                                            <input type="checkbox" name="obj_type" value="<?=$value;?>"
                                                   id="checkbox<?=$index;?>">
                                            <label for="checkbox<?=$index;?>"><?=$value?></label>
                                        </div>

                                        <? $index++;
                                        endforeach; ?>




                                    </div>

                                </div>

                                <div class="filter-row">

                                    <div class="col col-1">

                                        <h3>Особенность</h3>

                                    </div>

                                    <div class="col col-2">

                                        <? foreach ($obj_status_arr as $value):

                                            if ($value = "ready") {
                                                $name = 'Готовые';
                                            } else if ($value = "in_progress") {
                                                $name = 'Строящиеся';
                                            }
                                            ?>

                                            <div class="checkbox-box">
                                                <input type="checkbox" name="obj_status" value="<?= $value ?>"
                                                       id="checkbox<?= $index; ?>">
                                                <label for="checkbox<?= $index; ?>"><?= $name; ?></label>
                                            </div>
                                            <? $index++;
                                        endforeach; ?>




                                    </div>

                                </div>

                                <div class="filter-button">

                                    <button type="button" class="filter-btn">Подобрать</button>

                                    <button type="button" class="reset-filter-btn">сбросить</button>

                                </div>

                            </form>

                        </div>

                    </div>

                    <div class="show-hide-button">

                        <button type="button" class="show-hide-filter-btn"><i class="icon-chewron-up"
                                                                              aria-hidden="true"></i><span
                                class="filtershow-btn-txt">Развернуть</span></button>

                    </div>

                </div>

            </div>

        </section>

        <?php get_template_part('template-parts/parts/map'); // подколючаем карту "наших" обьектов ?>

        <section class="sortable-thumbnails-sect">

            <div class="row">

                <div class="sortable-header clearfix">


                    <div class="count-sort right">

                        <p>Всего: <?=$count;?></p>

                    </div>

                </div>

                <div class="sortable-thumbnails">


                    <?


                    if ($_GET && !empty($_GET)) { // если было передано что-то из формы

                        $obj_type = $_GET['type'];
                        $obj_status = $_GET['status'];

                        debug_to_console(  $obj_type );
                        debug_to_console(  $obj_status );

                         if($obj_type){
                             $obj_type_arr = explode("_", $obj_type);
                         }


                        if($obj_status){
                        $obj_status_arr = explode("_", $obj_status);
                        }

                    }




                    $query = new WP_Query(array(

                        'post_type' => 'objects',
                        'posts_per_page' => 12,

                        'meta_query' => array(
                            'relation' => 'AND',
                            array(
                                'key' => 'obj_type',
                                'value' => $obj_type_arr,
                                'compare' => 'IN'
                            ),
                            array(
                                'key' => 'статус_готовности_объекта',
                                'value' => $obj_status_arr,
                                'compare' => 'IN'
                            )
                        ),

                        'paged' => $paged,

                    ));

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              ?>






                    <h2>Дома</h2>

                    <div class="thumbnails-projects catalog offset-ziro visible-important">



                        <?    // вывод постов


                        // цикл
                        while ($query->have_posts()) :
                            $query->the_post(); ?>


                            <div class="thumbnail-box">

                                <div class="thumbnail">

                                    <div class="photo-box">

                                        <div class="photo">

                                            <? the_post_thumbnail('medium'); ?>

                                        </div>

                                        <div class="mask">

                                            <a  href="<? the_permalink(); ?>" class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                        </div>

                                    </div>

                                    <div class="thumbnail-txt">

                                        <p class="p-1"><? the_title(); ?></p>

                                        <p class="p-2"><? the_field('all_square');?> м<span class="sq">2</span></p>

                                        <p class="p-3 bold-txt">от <span><? the_field('price');?></span> <span class="rub">р.</span></p>

                                    </div>

                                </div>

                            </div>



                        <? endwhile; ?>

                        <?php wp_reset_query(); ?>







                    </div>

                    <div class="pagination-box offset-ziro">
                        <div class="pagination col col-1">

                            <? wp_pagenavi(array('query' => $query)); ?>



                        </div>

                        <div class="col col-2 clearfix">

                            <button class="show-all-btn right">показать всe</button>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section>

            <div class="row">






                <? the_content(); ?>




            </div>

        </section>


        <? // форма заказа дома мечты
          get_template_part('template-parts/parts/footer-order-form');
        ?>

      

    </div>
    <!-- /Content -->
<?php
get_footer();
