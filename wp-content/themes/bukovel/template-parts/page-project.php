<?php

/* Template Name: Шаблон проекты*/


get_header(); ?>
	<!-- Content -->
		<div class="content">

			<!--  Popus  -->

			<div class="popup-calback">
				
				<div class="popup-bg"></div>

				<div class="callback-box">

					<div class="close-btn-box">

						<button class="close-popup-btn" type="button"><i class="close-x-icon"></i><span>Закрыть окно</span></button>

					</div>		

					<div class="callback-content">

						<div class="sect-h">
					
							<h3 class="center-txt">заказ</h3>

							<h2 class="center-txt green-txt">звонка</h2>

						</div>

						<p>Оставьте ваши контактные данные, и наши менеджеры свяжутся с вами в ближайшее время, чтобы обсудить все детали.</p>

						<div class="callback-form">

							<form>

								<div class="input-box">
									
									<i class="fa fa-user-o" aria-hidden="true"></i>
									<input type="text" placeholder="Ваше имя">

								</div>

								<div class="input-box">
									
									<i class="fa fa-mobile" aria-hidden="true"></i>
									<input type="tel" placeholder="Ваш телефон">

								</div>

								<div class="input-box btn-box">
									
									<button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Отправить</button>

								</div>

							</form>

						</div>

					</div>

				</div>

			</div>
			<section class="inner-page-sect-1 show-main-float-nav-coor">
				
				<div class="row">
					
					<div class="breadcrumbs">
						
						<ul class="breadcrumbs-list">
							
							<li><a class="breadcrumb-link" href="/">Главная</a></li>

							<li><a class="breadcrumb-link" href="#">Проекты</a></li>

						</ul>

					</div>

					<div class="inner-h">
						
						<h2>Проекты</h2>

					</div>

				</div>

			</section>

			<section class="filter-section">
				
				<div class="row filter">
					
					<div class="filter-h-box">

						<h2 class="filter-h">Фильтр</h2>

					</div>

					<div class="filter-box">

						<div class="filters-wrapp">

							<div class="filters-content">

								<form>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Тип объекта</h3>

										</div>

										<div class="col col-2">
											
											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox1">
												<label for="checkbox1">Одноэтажные дома</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox2">
												<label for="checkbox2">Двухэтажные дома</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox3">
												<label for="checkbox3">Коттеджи</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox4">
												<label for="checkbox4">Шале</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox5">
												<label for="checkbox5">Фахверк</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox6">
												<label for="checkbox6">Хай-тек</label>

											</div>

										</div>

									</div>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Особенность</h3>

										</div>

										<div class="col col-2">
											
											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox7">
												<label for="checkbox7">С террасой</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox8">
												<label for="checkbox8">С мансардой</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox9">
												<label for="checkbox9">С эркером</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox10">
												<label for="checkbox10">С балконом</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox11">
												<label for="checkbox11">С бассейном</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox12">
												<label for="checkbox12">С гаражом</label>

											</div>

										</div>

									</div>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Площадь</h3>

										</div>

										<div class="col col-2">
											
											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox13">
												<label for="checkbox13">До 200 кв.м.</label>

											</div>

											<div class="checkbox-box">

												<input type="checkbox" name="filteritem" id="checkbox14">
												<label for="checkbox14">Более 200 кв.м.</label>

											</div>

										</div>

									</div>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Цена</h3>

										</div>

										<div class="col col-2 with-price-slider">
											
											<div class="price-slider-box">

												<div id="slider"></div>

												<div class="ranges-box clearfix">

													<div class="range-input"><p>от</p><input type="text" class="range-1"></div>

													<div class="range-input"><p>до</p><input type="text" class="range-2"></div>

												</div>

											</div>

											<div class="price-sorts">
												
												<div class="checkbox-box">

													<input type="checkbox" name="filteritem" id="checkbox15">
													<label for="checkbox15">Все</label>

												</div>

												<div class="checkbox-box">

													<input type="checkbox" name="filteritem" id="checkbox16">
													<label for="checkbox16">Недорогие</label>

												</div>

												<div class="checkbox-box">

													<input type="checkbox" name="filteritem" id="checkbox17">
													<label for="checkbox17">Элитные</label>

												</div>

											</div>

										</div>

									</div>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Количество комнат</h3>

										</div>

										<div class="col col-2">
											
											<div class="radio-box">

												<input type="radio" name="filterradio1" id="for1">
												<label for="for1">2</label>

											</div>

											<div class="radio-box">

												<input type="radio" name="filterradio1" id="for2">
												<label for="for2">3</label>

											</div>

											<div class="radio-box">

												<input type="radio" name="filterradio1" id="for3">
												<label for="for3">4</label>

											</div>

											<div class="radio-box">

												<input type="radio" name="filterradio1" id="for5">
												<label for="for5">5+</label>

											</div>

										</div>

									</div>

									<div class="filter-row">
									
										<div class="col col-1">
											
											<h3>Количество комнат</h3>

										</div>

										<div class="col col-2">

											<div class="radio-box">

												<input type="radio" name="filterradio2" id="for6">
												<label for="for6">1</label>

											</div>
											
											<div class="radio-box">

												<input type="radio" name="filterradio2" id="for7">
												<label for="for7">2</label>

											</div>

											<div class="radio-box">

												<input type="radio" name="filterradio2" id="for8">
												<label for="for8">3</label>

											</div>

										</div>

									</div>

									<div class="filter-button">
									
										<button type="button" class="filter-btn">Подобрать</button>

										<button type="button" class="reset-filter-btn">сбросить</button>

									</div>

								</form>

							</div>

						</div>

						<div class="show-hide-button">

							<button type="button" class="show-hide-filter-btn"><i class="icon-chewron-up" aria-hidden="true"></i><span class="filtershow-btn-txt">Развернуть</span></button>

						</div>

					</div>

				</div>

			</section>

			<section class="sortable-thumbnails-sect">
				
				<div class="row">

					<div class="sortable-header offset-ziro">
					
						<div class="sortable-links">
							
							<h3>Сортировать по:</h3>

							<ul class="sortable-links-list">

								<li><a class="sortable-link active" href="#">Популярности</a></li>
								<li><a class="sortable-link" href="#">Цене</a></li>
								<li><a class="sortable-link" href="#">Площади</a></li>
								<li><a class="sortable-link" href="#">Размеру</a></li>

							</ul>

						</div>

						<div class="count-sort">
							
							<p>Всего: 60</p>

						</div>

					</div>

					<div class="sortable-thumbnails">
						
						<h2>Одноэтажные дома</h2>

						<div class="thumbnails-projects catalog offset-ziro visible-important offset-ziro">
						
							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 12.22</p>

										<p class="p-2">134,5 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_2.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 11.88</p>

										<p class="p-2">190,3 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_3.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 45.66</p>

										<p class="p-2">144.8 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_4.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 11.88</p>

										<p class="p-2">134,5 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_5.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 18.00</p>

										<p class="p-2">190,3 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_6.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 00.11</p>

										<p class="p-2">144.8 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_7.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 12.22</p>

										<p class="p-2">134,5 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_8.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 11.88</p>

										<p class="p-2">190,3 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_9.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 45.66</p>

										<p class="p-2">144.8 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_10.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 11.88</p>

										<p class="p-2">134,5 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_11.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 18.00</p>

										<p class="p-2">190,3 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

							<div class="thumbnail-box">
								
								<div class="thumbnail">
									
									<div class="photo-box">

										<div class="photo">

											<img src="<?php bloginfo('template_url'); ?>/projects/photo_12.jpg" alt="">

										</div>

										<div class="mask">
											
											<a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

										</div>

									</div>

									<div class="thumbnail-txt">

										<p class="p-1">Проект дома 00.11</p>

										<p class="p-2">144.8 м<span class="sq">2</span></p>

										<p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

									</div>

								</div>

							</div>	

						</div>

						<div class="pagination-box offset-ziro">
							
							<div class="pagination col col-1">
								
								<ul class="pagination-list">
									
									<li><a href="#" class="pagination-link active">1</a></li>
									<li><a href="#" class="pagination-link">2</a></li>
									<li><a href="#" class="pagination-link">3</a></li>
									<li><a href="#" class="pagination-link">4</a></li>

								</ul>

							</div>

							<div class="col col-2 clearfix">
								
								<button type="button" class="show-all-btn right">показать всe</button>

							</div>

						</div>

					</div>

				</div>

			</section>

			<section>
				
				<div class="row">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content();?>
					<?php endwhile; endif; ?>
				</div>

			</section>


			<section class="sect-10">
				
				<div class="row sect_10_block">
					
					<div class="sect-h">
						
						<h3 class="white-txt">Закажите дом</h3>
						<h2>Своей мечты</h2>

					</div>

					<p class="center-txt font-16">Оставьте вашу заявку и в ближайшее время мы составим персональный и бесплатный проект на ваш дом.</p>

					<div class="send-order-form-box offset-ziro">
						<?php echo do_shortcode('[contact-form-7 id="59" title="Форма не в попапе"]');?>
					</div>

				</div>

			</section>

		</div>
	<!-- /Content -->
	<script type="text/javascript">
		var sliderPrice = document.getElementById("slider");

			var minRange = 0;

			var maxRange = 9000000;

			var offsetLeftRangeLeft;

			var offsetLeftRangeRight;

			noUiSlider.create(sliderPrice, {
				start: [894580, 5831600],
				connect: true,
				tooltips: true,
				step: 1,
				range: {
					'min': minRange,
					'max': maxRange
				}

			});

			sliderPrice.noUiSlider.on('update', function( values, handle ) {

				$(".noUi-tooltip:eq(0)").text( parseInt( $(".noUi-tooltip:eq(0)").text() ) );

				$(".noUi-tooltip:eq(1)").text( parseInt( $(".noUi-tooltip:eq(1)").text() ) );

				$(".range-1").val( parseInt( values[0] ) );

				$(".range-2").val( parseInt( values[1] ) );

			});


			$(".range-1").keyup(function() {

				if ( /[0-9.]/.test( $(".range-1").val() ) == true && 
					$(".range-1").val() < parseInt( $(".noUi-tooltip:eq("+ 1 +")").text() ) && 
				 	$(".range-1").val() <= maxRange ) {

					console.log( /[0-9.]/.test( $(".range-1").val() ) );

					noUiconnectPosition();

					$(".noUi-tooltip:eq("+ 0 +")").text( $(".range-1").val() );

					$(".noUi-origin:eq("+ 0 +")").animate({"left" : offsetLeftRangeLeft + "%"}, 300);

				}

			});

			$(".range-2").keyup(function() {

				if ( /[0-9.]/.test( $(".range-2").val() ) == true && 
					$(".range-2").val() > parseInt( $(".noUi-tooltip:eq("+ 0 +")").text() ) && 
				 	$(".range-2").val() <= maxRange ) {

					noUiconnectPosition();

					$(".noUi-tooltip:eq("+ 1 +")").text( $(".range-2").val() );

					$(".noUi-origin:eq("+ 1 +")").animate({"left" : offsetLeftRangeRight + "%"}, 300);

				}

			});

			function noUiconnectPosition() {

				offsetLeftRangeLeft = parseInt( $(".range-1").val() ) / ( maxRange / 100 );

				offsetLeftRangeRight = parseInt( $(".range-2").val() ) / ( maxRange / 100 );

				$(".noUi-connect").animate({
											"left" : offsetLeftRangeLeft + "%",
											"right" : ( 100 - offsetLeftRangeRight ) + "%"
										}, 300);

			}
	</script>
<?php
get_footer();
