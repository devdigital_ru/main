<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bukovel
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/vendors/css/animate.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/style.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/font-awesome.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/responsive.css">

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/slick/slick.css">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendors/css/jquery.mCustomScrollbar.css">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendors/css/nouislider.css">

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendors/css/nouislider.tooltips.css">

<link href="<?php bloginfo('template_url'); ?>/vendors/css/select2.css" rel="stylesheet" />

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/vendors/js/jquery.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/slick/slick.js"></script>

<script src="<?php bloginfo('template_url'); ?>/vendors/js/nouislider.js"></script>

<script src="<?php bloginfo('template_url'); ?>/vendors/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="<?php bloginfo('template_url'); ?>/vendors/js/select2.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/vendors/js/wow.min.js"></script>
<script>new WOW().init();</script>


<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon/favicon.ico" sizes="16x16">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
	if(is_front_page()){
		get_header('main');
	}else{
		get_header('inner');
	}
?>
