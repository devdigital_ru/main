<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bukovel
 */

get_header(); ?>
    <!-- Content -->
    <div class="content">








        <section class="sect-1 show-main-float-nav-coor">

            <div class="for-resp-slideinfo row"></div>

            <div class="slider-main slider_1">
                <?php
                $args_big = array(
                    'post_type' => 'promo',
                    'posts_per_page' => 5
                );
                // Запрос. $args - параметры запроса
                query_posts($args_big);
                // Цикл WordPress
                if (have_posts()):
                    while (have_posts()) : the_post(); ?>
                        <div>

                            <?php the_post_thumbnail(); ?>

                            <div class="row">

                                <div class="slide-info">

                                    <a href="/o-kompanii/akcii#promo_<?php echo get_the_ID(); ?>"
                                       class="morelink-slide"><i class="slide-icon-more"></i>Подробнее о предложении</a>

                                    <h3><?php the_field('заголовок_1'); ?></h3>

                                    <h2><?php the_field('заголовок_2'); ?></h2>

                                </div>

                            </div>

                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                <?php endif; ?>
            </div>
            <div class="slide-bottom-row row offset-ziro">

                <div class="slick-dots-append for-slider-main"></div>

                <div class="for-tab">

                    <div class="slide-info-box">

                        <div class="slide-info change-content">
                            <?php
                            $args_big = array(
                                'post_type' => 'promo',
                                'posts_per_page' => 1
                            );
                            // Запрос. $args - параметры запроса
                            query_posts($args_big);
                            // Цикл WordPress
                            if (have_posts()):
                                while (have_posts()) : the_post(); ?>
                                    <a href="/o-kompanii/akcii#promo_<?php echo get_the_ID(); ?>"
                                       class="morelink-slide"><i class="slide-icon-more"></i>Подробнее о предложении</a>

                                    <h3><?php the_field('заголовок_1'); ?></h3>

                                    <h2><?php the_field('заголовок_2'); ?></h2>
                                <?php endwhile; ?>
                                <?php wp_reset_query(); ?>
                            <?php endif; ?>
                        </div>

                    </div>

                </div>

            </div>

            <!-- <div class="for-resp-slidei-btn row"></div> -->

        </section>

        <section class="sect-2">


            <?// section 2 data

            $block_title = get_field('block_title', 42);
            $title = get_field('about_us_title', 42);
            $text = get_field('about_us_text', 42);
            $about_us_img = get_field('about_us_img', 42);
            $link = get_field('about_us_link', 42);


            ?>

            <div class="row">

                <div class="sect-h-box">

                    <?= $block_title; ?>

                </div>

                <div class="article two-cols offset-ziro">

                    <div class="col col-1 tab-novisible">

                        <div class="float-box">

                            <?php if (!empty($about_us_img)): ?>

                                <img src="<?php echo $about_us_img['url']; ?>"
                                     alt="<?php echo $about_us_img['title']; ?>"/>

                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="col col-2">

                        <h4><?= $title; ?></h4>

                        <p><?= $text; ?></p>

                        <a class="more-link" href="<?= $link; ?>"><i class="more-see-icon"></i>Подробнее о нас</a>

                    </div>

                </div>

            </div>

        </section>

        <section class="sect-3">

            <div class="row projects-sect">

                <h3 class="h-sect center-txt">Проекты</h3>
                <h2 class="h-sect center-txt green-txt">из клееного бруса</h2>

                <ul class="project-views-list">

                    <li><a class="project-views-link" href="#">домов</a></li>
                    <li><a class="project-views-link active" href="#">Бань</a></li>

                </ul>

                <div class="thumbnails-projects offset-ziro">

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 12.22</p>

                                <p class="p-2">134,5 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_2.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_3.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 45.66</p>

                                <p class="p-2">144.8 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_4.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">134,5 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_5.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 18.00</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_6.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 00.11</p>

                                <p class="p-2">144.8 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="our-projects-slider-box">

                    <div class="our-projects-slider thumbnails-projects">

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 12.22</p>

                                    <p class="p-2">134,5 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_2.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_3.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 45.66</p>

                                    <p class="p-2">144.8 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_4.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">134,5 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 194 530</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_5.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 18.00</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_6.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 00.11</p>

                                    <p class="p-2">144.8 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="our-projects-slider-arrows"></div>

                </div>

                <div>

                    <button class="all-projects"><i class="fa fa-bars" aria-hidden="true"></i>Все проекты</button>

                </div>

            </div>

        </section>

        <section class="sect-4">

            <div class="row">

                <div class="for-resp-advantages-h">

                    <div class="advantages-h">

                        <?php the_field('заголовок_преимущества', 42); ?>

                    </div>

                </div>

                <div class="advantages offset-ziro">
                    <?php
                    $count = 1;
                    while (have_rows('преимущества_список', 42)): the_row(); ?>
                        <?php
                        $title = get_sub_field('заголовок');
                        $text = get_sub_field('описание');
                        $classp = get_sub_field('клас_иконки');
                        ?>
                        <div class="col">

                            <div class="icon <?php echo $classp; ?>"></div>

                            <div>

                                <h3><?php echo $title; ?></h3>

                                <p><?php echo $text; ?></p>

                            </div>

                        </div>
                        <?php if ($count == 3): ?>
                            <div class="advantages-h">
                                <?php the_field('заголовок_преимущества', 42); ?>
                            </div>
                        <?php endif; ?>
                        <?php
                        $count++;
                    endwhile; ?>
                </div>

            </div>

        </section>

        <section class="sect-5">


            <? // section-5 data
            $techno_title = get_field('techno_title', 42);
            $techno_subtitle = get_field('techno_subtitle', 42);
            $techno_text = get_field('techno_text', 42);
            $techno_text_image = get_field('techno_text_image', 42);
            $techno_image = get_field('techno_image', 42);
            $techno_link = get_field('techno_link', 42);


            ?>

            <div class="row">

                <div class="sect-h-box sect-h-5 clearfix">

                    <?= $techno_title; ?>

                </div>

                <div class="artcile-sect-5 clearfix">

                    <? if ($techno_subtitle): ?>

                        <h4 class="main-h center-txt"><?= $techno_subtitle; ?></h4>

                    <? endif; ?>


                    <div class="img-box resp circle">
                        <?php if (!empty($techno_image)): ?>
                            <img src="<?php echo $techno_image['url']; ?>"/>
                        <?php endif; ?>
                    </div>

                    <p><?= $techno_text; ?></p>

                    <div class="img-box circle tab-novisible left">

                        <?php if (!empty($techno_image)): ?>
                            <img src="<?= $techno_image['url']; ?>"/>
                        <?php endif; ?>

                    </div>
                    <? if ($techno_text_image): ?>
                        <?= $techno_text_image; ?>
                    <? endif; ?>



                    <? if ($techno_link): ?>
                        <a class="more-link" href="<?= $techno_link; ?>">Подробнее о технологии</a>
                    <? endif; ?>

                </div>

            </div>

        </section>


        <? if (get_field('video_title', 42)): ?>
            <section class="sect-6">

                <div class="row">

                    <div class="sect-h">

                        <?= get_field('video_title', 42); ?>

                    </div>

                    <p class="small"><?= get_field('video_descr', 42); ?></p>

                    <div class="show-video-link-box">

                        <a class="showvideo" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

                        <p>ролик откроется в новом окне</p>

                    </div>

                </div>

            </section>
        <? endif; ?>

        <section class="sect-our-objects">

            <div class="row">

                <div class="sect-h">

                    <h3>Наши объекты</h3>

                    <h2 class="green">из клееного бруса</h2>

                </div>

                <div class="objects offset-ziro">

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_7.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 12.22</p>

                                <p class="p-2">134,5 м<span class="sq">2</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_8.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 12.22</p>

                                <p class="p-2">134,5 м<span class="sq">2</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_9.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 12.22</p>

                                <p class="p-2">134,5 м<span class="sq">2</span></p>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="our-objects-slider-box">

                    <div class="our-objects-slider">

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_7.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 12.22</p>

                                    <p class="p-2">134,5 м<span class="sq">2</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_8.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 12.22</p>

                                    <p class="p-2">134,5 м<span class="sq">2</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_9.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="more_about_project_icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 12.22</p>

                                    <p class="p-2">134,5 м<span class="sq">2</span></p>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="our-objects-slider-arrows"></div>

                </div>

                <div>

                    <button class="all-projects"><i class="fa fa-bars" aria-hidden="true"></i>Все наши объекты</button>

                </div>

            </div>

        </section>

        <section class="sect-7">

            <div class="row map-section">

                <div class="map-content">

                    <div class="sect-h">

                        <h3>Карта</h3>

                        <h2 class="green">наших объектов</h2>

                    </div>

                    <div class="map-object-info">

                        <button type="button" class="close-map-object-info"><i class="close-x-icon"></i></button>

                        <div class="object-info-append">

                            <a href="#" class="object-info">

                                <div class="object-photo-box">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                </div>

                                <div class="map-object-info-content">

                                    <h3>Коттеджный поселок</h3>

                                    <h3>«Никольское-Лесное»</h3>

                                    <p>Ленинградское шоссе, 26 км</p>

                                </div>

                            </a>

                        </div>

                    </div>

                    <button class="show-map-btn"><i class="fa fa-map-marker" aria-hidden="true"></i><span
                            class="map-btn-txt">Раскрыть карту объектов</span></button>

                </div>

                <div class="map-box">

                    <img class="map-img" src="<?php bloginfo('template_url'); ?>/img/map.jpg" alt="">


                    <div class="marker-box marker-1">

                        <i class="map-marker-icon"></i>

                        <div class="object-info-for-append">

                            <a href="" class="object-info">

                                <div class="object-photo-box">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                </div>

                                <div class="map-object-info-content">

                                    <h3>Коттеджный поселок</h3>

                                    <h3>«Никольское-Лесное»</h3>

                                    <p>Ленинградское шоссе, 26 км</p>

                                </div>

                            </a>

                        </div>

                    </div>

                    <div class="marker-box marker-2">

                        <i class="map-marker-icon"></i>

                        <div class="object-info-for-append">

                            <a href="#" class="object-info">

                                <div class="object-photo-box">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                </div>

                                <div class="map-object-info-content">

                                    <h3>Объект 2</h3>

                                    <h3>«Никольское-Лесное»</h3>

                                    <p>Ленинградское шоссе, 26 км</p>

                                </div>

                            </a>

                        </div>

                    </div>

                    <div class="marker-box marker-3">

                        <i class="map-marker-icon"></i>

                        <div class="object-info-for-append">

                            <a href="#" class="object-info">

                                <div class="object-photo-box">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_1.jpg" alt="">

                                </div>

                                <div class="map-object-info-content">

                                    <h3>Объект 3</h3>

                                    <h3>«Никольское-Лесное»</h3>

                                    <p>Ленинградское шоссе, 26 км</p>

                                </div>

                            </a>

                        </div>

                    </div>

                </div>


            </div>

        </section>


        <?// section 8 data
        $adv_title = get_field('adv_title', 42);
        $adv_descr = get_field('adv_descr', 42);

        //adv_plus row
        //         adv_plus_title  adv_plus_descr
        //adv_minus row
        //         adv_minus_title  adv_minus_descr


        ?>

        <section class="sect-8">


            <div class="row">

                <div class="sect-h">

                    <h3>Преимущества домов</h3>

                    <h2 class="green">Из клееного бруса</h2>

                </div>

                <div class="center-txt">

                    <p class="font-20">Клееный брус часто используется в строительстве малоэтажных домов. Многие
                        считают, что этот материал является наилучшим</p>

                    <p class="font-18">Тем не менее, и у него есть свои минусы. Давайте же разберемся в преимуществах и
                        достоинствах клееного бруса.</p>

                </div>

                <div class="accordion-sect two-cols offset-ziro">


                    <?// ------  преимущество --------- ?>

                    <div class="accordion plus col-1">

                        <div class="accordion-h">

                            <div class="inner-col-1">

                                <div class="fa-box">

                                    <i class="fa fa-plus" aria-hidden="true"></i>

                                </div>

                            </div>

                            <div class="inner-col-2">

                                <h2 class="slide-btn" id="slide_block_1">Преиущества<br/> клееного бруса</h2>

                            </div>

                        </div>

                        <? if (have_rows('adv_plus', 42)): ?>

                            <div class="slide-block-wrapp" id="drop_slide_block_1">

                                <div class="slide-block accordion-dropdown">


                                    <? while (have_rows('adv_plus', 42)) : the_row(); ?>


                                        <div class="accordion-item">

                                            <div class="inner-col-1">

                                                <p class="item-num">01</p>

                                            </div>

                                            <div class="inner-col-2">

                                                <div class="dropdown-click">

                                                    <button class="dropdown-btn"><i class="fa fa-sort-asc"
                                                                                    aria-hidden="true"></i></button>

                                                    <h3> <? the_sub_field('adv_plus_title'); ?></h3>

                                                </div>

                                                <div class="accordion-item-content">

                                                    <div class="acoordion-drop-wrapp">

                                                        <div class="acoordion-item-txt">

                                                            <p><? the_sub_field('adv_plus_descr'); ?></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    <? endwhile; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>


                    <?// ------  недостатки --------- ?>

                    <div class="accordion minus col-2">

                        <div class="accordion-h">

                            <div class="inner-col-1">

                                <div class="fa-box minus">

                                    <i class="fa fa-minus" aria-hidden="true"></i>

                                </div>

                            </div>

                            <div class="inner-col-2">

                                <h2 class="slide-btn" id="slide_block_2">Недостаток</h2>

                            </div>

                        </div>

                        <? if (have_rows('adv_minus', 42)): ?>

                            <div class="slide-block-wrapp" id="drop_slide_block_2">

                                <div class="slide-block accordion-dropdown">


                                    <? while (have_rows('adv_minus', 42)) : the_row(); ?>


                                        <div class="accordion-item">

                                            <div class="inner-col-1">

                                                <p class="item-num">01</p>

                                            </div>

                                            <div class="inner-col-2">

                                                <div class="dropdown-click">

                                                    <button class="dropdown-btn"><i class="fa fa-sort-asc"
                                                                                    aria-hidden="true"></i></button>

                                                    <h3> <? the_sub_field('adv_minus_title'); ?></h3>

                                                </div>

                                                <div class="accordion-item-content">

                                                    <div class="acoordion-drop-wrapp">

                                                        <div class="acoordion-item-txt">

                                                            <p><? the_sub_field('adv_minus_descr'); ?></p>


                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    <? endwhile; ?>
                                </div>
                            </div>
                        <? endif; ?>

                    </div>

                </div>


            </div>

        </section>

        <section class="sect-9">







            <div class="row testimonial-sect">

                <div class="header-testimonial-slider offset-ziro">

                    <div class="col col-1 offset-ziro">

                        <div class="inner-col testimonial-slick-arrows">

                            <button class="leftSlickArrow"><i class="fa fa-caret-left" aria-hidden="true"></i></button>
                            <button class="rightSlickArrow"><i class="fa fa-caret-right" aria-hidden="true"></i>
                            </button>

                        </div>

                        <div class="inner-col">

                            <a href="/reviews/" class="more-testimonials"><i class="testimonial-icon"></i>Все отзывы</a>

                        </div>

                    </div>

                    <div class="col col-2">

                        <h3>Отзывы</h3>

                        <h2 class="green-txt">Наших клиентов</h2>

                    </div>

                </div>

                <div class="testimonial-slider-box">

                    <div class="testimonial-slider slider_2">



                        <?// отзывы
                        $args_rev = array(
                            'post_type' => 'reviews',
                            'posts_per_page' => -1
                        );
                        // Запрос. $args - параметры запроса
                        query_posts($args_rev);
                        // Цикл WordPress
                        if (have_posts()):
                            while (have_posts()) : the_post(); ?>

                                <div class="testimonial">

                                    <h3><?php the_title(); ?></h3>

                                    <p><?php the_field('city'); ?></p>

                                    <div class="testimonial-txt">

                                        <?php the_content(); ?>

                                        <a href="<?=get_permalink();?>" class="read-more"><i class="fa fa-comment-o" aria-hidden="true"></i>Читать
                                            далее...</a>

                                    </div>

                                </div>



                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        <?php endif; ?>


                    </div>

                    <div class="testimonial-slick-arrows-bottom">

                        <div>

                            <button class="leftSlickArrow"><i class="fa fa-caret-left" aria-hidden="true"></i></button>
                            <button class="rightSlickArrow"><i class="fa fa-caret-right" aria-hidden="true"></i>
                            </button>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <? // форма заказа дома мечты
        get_template_part('template-parts/parts/footer-order-form');
        ?>

    </div>
    <!-- /Content -->
<?php
//get_sidebar();
get_footer();
