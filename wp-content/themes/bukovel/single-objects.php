<?php
get_header(); ?>
    <!-- Content -->

    <div class="content">

        <?
        //получим все нужные данные здесь
        $fields = get_fields();
        
        ?>






        <section class="inner-page-sect-1 show-main-float-nav-coor">

            <div class="row">

                <div class="breadcrumbs">

                    <ul class="breadcrumbs-list">

                        <li><a class="breadcrumb-link" href="#">Главная</a></li>

                        <li><a class="breadcrumb-link" href="#">Проекты</a></li>

                        <li><a class="breadcrumb-link" href="#">Одноэтажные дома</a></li>

                        <li><a class="breadcrumb-link" href="#">Проект СП-237</a></li>

                    </ul>

                </div>


                <? var_dump($fields);  ?>

                <div class="inner-h">

                    <h2>Проект Сп-237</h2>

                </div>

            </div>

        </section>

        <section class="slider-project-sect">

            <div class="row">

                <div class="project-slider-box slider_3 offset-ziro">

                    <div class="project-slider col col-1 project-slider-col">

                        <div class="project-big-slider">
                            <? foreach ($fields['photos'][0] as $photo) { ?>
                                <div><img src="<?= $photo['url'] ?>" alt=""></div>
                                <?
                            } ?>

                        </div>

                        <div class="project-miniatures-slider">
                            <? foreach ($fields['photos'][0] as $photo) { ?>
                                <div><img src="<?= $photo['sizes']['thumbnail'] ?>" alt=""></div>
                                <?
                            } ?>

                        </div>

                    </div>

                    <div class="project-info-box col col-2 project-slider-col">

                        <div class="project-info">

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-1"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Общая площадь</h3>
                                    <p><?= $fields['all_square']; ?> м<span class="sq-2">2</span></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-2"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Площадь террас(ы)</h3>

                                    <p><?= $fields['any_square']; ?> м<span class="sq-2">2</span></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-3"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество этажей</h3>

                                    <p><?= $fields['floors']; ?></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-4"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество спален</h3>

                                    <p><?= $fields['bedrooms']; ?></p>

                                </div>

                            </div>

                            <div class="project-info-row">

                                <div class="col col-1">

                                    <i class="project-info-icon icon-5"></i>

                                </div>

                                <div class="col col-2">

                                    <h3>Количество санузлов</h3>

                                    <p><?= $fields['bathrooms']; ?></p>

                                </div>

                            </div>

                            <p class="project-price"><?= number_format($fields['price'], 0, '.', ' '); ?> <span
                                    class="rub">Р.</span></p>

                            <div class="project-info-btn-box">

                                <button type="submit" class="send-btn call-callback"><i class="icon call-back-icon"
                                                                                        aria-hidden="true"></i>Отправить
                                    заявку
                                </button>

                                <button class="credit-btn"><i class="icon credit-icon"></i><span class="credit-link">Заказать кредит</span>
                                </button>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section class="tabs-sect">

            <div class="row">

                <div class="tabs">

                    <div class="row">

                        <label for="tab1" class="card_tab_link active">Описание</label>
                        <label for="tab2" class="card_tab_link">Планировки</label>
                        <label for="tab3" class="card_tab_link">Состав работ</label>
                        <label for="tab4" class="card_tab_link">Гарантии</label>

                    </div>

                    <div class="tabs-content">

                        <input type="radio" name="tab" id="tab1">
                        <div id="tab1" class="card_tab">

                            <?= $fields['description']; ?>

                        </div>

                        <input type="radio" name="tab" id="tab2">
                        <div id="tab2" class="card_tab">
                            <?
                            foreach ($fields['plains'] as $plain) { ?>
                            <div class="plans-box">

                                <div class="plan col">

                                    <div class="plan-img-box">
                                        <img src="<?=$plain['plain_img']['url'];?>" alt="">
                                    </div>

                                    <div class="plan-info">

                                        <div class="plan-info-header">

                                            <h3>Площадь <?=$plain['floor_value'];?> этажа</h3>

                                            <p><?=$plain['floor_square'];?> м<span class="sq">2</span></p>

                                        </div>

                                        <?
                                        if(!empty($plain['rooms_square'])){
                                            foreach ($plain['rooms_square'] as $square) {?>
                                                <div class="plan-info-table">

                                                    <div class="plan-info-table-row">

                                                        <div class="cell cell-1">

                                                            <h3><?=$square['rooms_name'];?></h3>

                                                        </div>

                                                        <div class="cell cell-2">

                                                            <p><?=$square['rooms_square'];?> м<span class="sq-2">2</span></p>

                                                        </div>

                                                    </div>

                                                </div>
                                            <? }
                                        }
                                        ?>
                                    </div>

                                </div>
                                <? } ?>



                            </div>


                        </div>

                        <input type="radio" name="tab" id="tab3">
                        <div id="tab3" class="card_tab">

                            <div class="tab-table two-cols">
                                <? foreach ($fields['job_description'] as $field) {?>
                                    <div class="tab-col col-1">

                                        <div class="header-col">

                                            <h3>Комплектация</h3>

                                            <p><?=$field['equipment'];?></p>

                                        </div>

                                        <div class="tab-txt">

                                            <h3>В состав комплекта входит:</h3>

                                            <ul class="with-mark">
                                                <? foreach ($field['estimate'] as $item) {?>
                                                    <li><?=$item['name'];?></li>
                                                <?}?>
                                            </ul>

                                            <h3 class="price-h">Стоимость:</h3>

                                            <p class="price-num"><?=number_format($fields['price']);?> <span class="rub">Р.</span></p>

                                        </div>

                                    </div>
                                <? }
                                ?>



                            </div>

                        </div>

                        <input type="radio" name="tab" id="tab4">
                        <div id="tab4" class="card_tab">

                            <h3>Таб 4</h3>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <section class="project-thumbnails-1-sect">

            <div class="row">

                <div class="thumbnails project-thumbnails-1 offset-ziro">

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-1"></i>

                        </div>

                        <p class="p-main">Собственное производство</p>

                        <p class="p-2">клееного бруса</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-2"></i>

                        </div>

                        <p class="p-main">Бесплатное</p>

                        <p class="p-2">проектирование</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-3"></i>

                        </div>

                        <p class="p-main">качественная</p>

                        <p class="p-2">доставка и монтаж</p>

                    </div>

                    <div class="thumbnail col">

                        <div class="icon-box">

                            <i class="icon icon-4"></i>

                        </div>

                        <p class="p-main">гарантия 5 лет</p>

                        <p class="p-2">на все работы</p>

                    </div>

                </div>

            </div>

        </section>

        <section class="simmilar-offers-sect">

            <div class="simmilar-offers row">

                <h2>Схожие предложения</h2>

                <div class="simmilar-offers-thumbnails thumbnails three-cols offset-ziro for-resp-desc">

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_10.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_11.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_12.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="simmilar-offers-slider-box for-resp-mob">

                    <div class="simmilar-offers-slider slider-7 slider-slick-with-arrows">

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_10.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_11.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_12.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="simmilar-offers-slider-arrows slick-append-arrows center-txt"></div>

                </div>

            </div>

        </section>

        <section class="last-saw-sect">

            <div class="last-saw row">

                <h2>Просмотренные ранее</h2>

                <div class="last-saw-thumbnails thumbnails offset-ziro for-resp-desc">

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_4.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_11.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_13.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                    <div class="thumbnail-box">

                        <div class="thumbnail">

                            <div class="photo-box">

                                <div class="photo">

                                    <img src="<?php bloginfo('template_url'); ?>/projects/photo_14.jpg" alt="">

                                </div>

                                <div class="mask">

                                    <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                </div>

                            </div>

                            <div class="thumbnail-txt">

                                <p class="p-1">Проект дома 11.88</p>

                                <p class="p-2">190,3 м<span class="sq">2</span></p>

                                <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="last-saw-thumbnails-slider-box for-resp-mob">

                    <div class="last-saw-thumbnails-slider slider-8">

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_4.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_11.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_13.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                        <div class="thumbnail-box">

                            <div class="thumbnail">

                                <div class="photo-box">

                                    <div class="photo">

                                        <img src="<?php bloginfo('template_url'); ?>/projects/photo_14.jpg" alt="">

                                    </div>

                                    <div class="mask">

                                        <a class="more-link"><i class="cube-icon"></i>Подробнее о проекте</a>

                                    </div>

                                </div>

                                <div class="thumbnail-txt">

                                    <p class="p-1">Проект дома 11.88</p>

                                    <p class="p-2">190,3 м<span class="sq">2</span></p>

                                    <p class="p-3 bold-txt">от <span>2 342 500</span> <span class="rub">р.</span></p>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="last-saw-thumbnails-slider-arrows center-txt"></div>

                </div>

            </div>

        </section>


        <section class="sect-10">

            <div class="row sect_10_block">

                <div class="sect-h">

                    <h3 class="white-txt">Закажите дом</h3>
                    <h2>Своей мечты</h2>

                </div>

                <p class="center-txt font-16">Оставьте вашу заявку и в ближайшее время мы составим персональный и
                    бесплатный проект на ваш дом.</p>

                <div class="send-order-form-box offset-ziro">

                    <form>

                        <div class="col col-1">

                            <div class="input-box input-trasparent">

                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                <input type="text" placeholder="Ваше имя">

                            </div>

                        </div>

                        <div class="col col-2">

                            <div class="input-box input-mob">

                                <i class="fa fa-mobile" aria-hidden="true"></i>
                                <input type="tel" placeholder="8 (921) 890-34-34">

                            </div>

                        </div>

                        <div class="col col-3">

                            <div class="input-btn-box">

                                <input type="button" value="отправить" class="input-btn-submit">

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </section>

    </div>
    <!-- /Content -->
    <script type="text/javascript">
        // ----------------------------------------------------------------

        $(".project-big-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            asNavFor: ".project-miniatures-slider"
        });

        $(".project-miniatures-slider").slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: ".project-big-slider",
            arrows: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });

        // ----------------------------------------------------------------

        $(".slider-7").slick({
            slidesToShow: 1,
            slidesToScroll: 1
        });

        // ----------------------------------------------------------------

        $(".slider-8").slick({
            slidesToShow: 1,
            slidesToScroll: 1
        });

        // ----------------------------------------------------------------
    </script>
<?php
get_footer();
